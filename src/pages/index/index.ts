import Vue from 'vue';
import App from './App.vue';
import './index.scss';
import 'plyr/dist/plyr.css'
import {images} from './images';
import VueRouter from 'vue-router'
import AnimeList from "./components/AnimeList.vue";
import EpisodeList from "./components/EpisodeList/EpisodeList.vue";
import VideoPlayer from "./components/VideoPlayer/VideoPlayer.vue";

Vue.use(VueRouter);

Vue.config.productionTip = false;


const routes = [
	{path: '/', name: 'home', component: AnimeList},
	{path: '/anime/:id', name: 'anime', component: EpisodeList},
	{path: '/anime/:id/:ep', name: 'episode', component: VideoPlayer}
];

const router = new VueRouter({routes});

new Vue({
	render: h => h(App),
	router
}).$mount('#app');

export {
	images
}
