import {Core, Kore} from "@kirinnee/core";
import {CanimeClient, ICanimeClient} from "../../classLibrary/CanimeClient";
import {LocalData} from "../../classLibrary/LocalData";

const shaka = require("shaka-player");
//Initialize core usage
let core: Core = new Kore();
core.ExtendPrimitives();

//Initialize monkey patching
declare  global {
	interface Date {
		toShortFormat(): string;
	}
}
Date.prototype.toShortFormat = function (): string {
	let month_names = ["January", "February", "March",
		"April", "May", "June",
		"July", "August", "September",
		"October", "November", "December"];
	
	let day = this.getDate();
	let month_index = this.getMonth();
	let year = this.getFullYear();
	
	return "" + day + " " + month_names[month_index] + " " + year;
};
declare var PRODUCTION: boolean;
declare var HOST: string;
//Setup Local data
let local: LocalData = new LocalData();
local.Load();

//Initialize client
let host: string = "https://" + HOST;
let client: ICanimeClient = new CanimeClient(core, local, host);
if (!PRODUCTION) {
	(window as any).local = local;
	(window as any).client = client;
}

//Initialize Shaka player
shaka.polyfill.installAll();

let $$ = (time: number): Promise<void> => new Promise<void>(r => setTimeout(r, time));
export {
	core,
	host,
	client,
	local,
	$$
}
