import {Anime} from "./Anime";
import {DetailedAnime} from "./DetailedAnime";
import {Episode} from "./Episode";
import {Core} from "@kirinnee/core";
import {LocalData} from "./LocalData";

interface ICanimeClient {
	
	GetAllAnime(page: number, amount: number): Promise<Anime[]>;
	
	GetAnime(guid: string): Promise<DetailedAnime>;
	
	GetEpisode(guid: string, episode: number): Promise<Episode>;
	
	GetAnimeName(guid: string): Promise<string>;
	
	FindAlternateImage(search: string): Promise<string>
	
	SearchAnime(search: string): Promise<Anime[]>
}

class CanimeClient implements ICanimeClient {
	async FindAlternateImage(search: string): Promise<string> {
		const query: any = {
			"query": "\nquery (\n    $page: Int = 1,\n    $type: MediaType,\n    $isAdult: Boolean = false,\n    $search: String,\n    $format: MediaFormat\n    $status: MediaStatus,\n    $countryOfOrigin: CountryCode,\n    $source: MediaSource,\n    $season: MediaSeason,\n    $year: String,\n    $onList: Boolean,\n    $yearLesser: FuzzyDateInt,\n    $yearGreater: FuzzyDateInt,\n    $licensedBy: [String],\n    $includedGenres: [String],\n    $excludedGenres: [String],\n    $includedTags: [String],\n    $excludedTags: [String],\n    $sort: [MediaSort] = [SCORE_DESC, POPULARITY_DESC]\n) {\n    Page (page: $page, perPage: 1) {\n        pageInfo {\n            total\n            perPage\n            currentPage\n            lastPage\n            hasNextPage\n        }\n        ANIME: media (\n            type: $type,\n            season: $season,\n            format: $format,\n            status: $status,\n            countryOfOrigin: $countryOfOrigin,\n            source: $source,\n            search: $search,\n            onList: $onList,\n            startDate_like: $year,\n            startDate_lesser: $yearLesser,\n            startDate_greater: $yearGreater,\n            licensedBy_in: $licensedBy,\n            genre_in: $includedGenres,\n            genre_not_in: $excludedGenres,\n            tag_in: $includedTags,\n            tag_not_in: $excludedTags,\n            sort: $sort,\n            isAdult: $isAdult\n        ) {\n            id\n            title {\n                userPreferred\n            }\n            coverImage {\n                large: extraLarge\n                color\n            }\n        }\n    }\n}\n",
			"variables": {"page": 1, "type": "ANIME", "sort": "SEARCH_MATCH", "search": search}
		};
		let image: Response = await fetch("https://graphql.anilist.co", {
			method: 'POST',
			body: JSON.stringify(query),
			headers: {
				"content-type": "application/json"
			}
		});
		let data = await image.json();
		console.log(data);
		try {
			return data.data.Page.ANIME[0].coverImage.large
		} catch (e) {
			return ''
		}
	}
	
	
	private readonly host: string;
	private readonly core: Core;
	private readonly cache: LocalData;
	private names: { [s: string]: string } = {};
	
	async GetAllAnime(page: number, amount: number): Promise<Anime[]> {
		let animes: any[] = await this.cache.fetch(`${this.host}/api/Anime?page=${page}&amount=${amount}`);
		animes.Each(e => this.names[e.id] = e.name);
		return animes.Map(e => new Anime(e.id, e.name, e.genre, e.rating, e.synopsis, e.updated, e.episode, e.image))
	}
	
	async GetAnime(guid: string): Promise<DetailedAnime> {
		let anime: any = await this.cache.fetch(`${this.host}/api/Anime/${guid}`);
		this.names[anime.id] = anime.name;
		return new DetailedAnime(anime.id, anime.name, anime.image,
			anime.genre, anime.rating, anime.synopsis, anime.episodes.Map((e: any) => this.ToEpisode(e)));
	}
	
	async SearchAnime(search: string): Promise<Anime[]> {
		let animes: any[] = await this.cache.fetch(`${this.host}/api/Anime/search?name=${search}`);
		animes.Each(e => this.names[e.id] = e.name);
		return animes.Map(e => new Anime(e.id, e.name, e.genre, e.rating, e.synopsis, e.updated, e.episode, e.image))
	}
	
	async GetEpisode(guid: string, episode: number): Promise<Episode> {
		let ep: any = await this.cache.fetch(`${this.host}/api/Anime/${guid}/${episode}`);
		return this.ToEpisode(ep);
	}
	
	private ToEpisode(input: any) {
		return new Episode(this.core, this.host, input.updated, input.name, input.number, input.streamLink, input.mkvLink, input.mp4Link);
	}
	
	constructor(core: Core, cache: LocalData, host: string) {
		core.AssertExtend();
		this.host = host;
		this.core = core;
		this.cache = cache;
	}
	
	async GetAnimeName(guid: string): Promise<string> {
		if (this.names[guid] != null)
			return this.names[guid];
		else {
			let a = await this.GetAnime(guid);
			return a.Name;
		}
	}
}

export {CanimeClient, ICanimeClient}
