class LocalData {
	private lmock: { getItem: Function, setItem: Function } =
		{
			getItem: () => {},
			setItem: () => {}
		};
	private episodeVisited: { [s: string]: { [s: number]: boolean } } = {};
	private episodeSubscribed: { [s: string]: boolean } = {};
	private fetchCache: { [s: string]: { time: number, data: any } } = {};
	private askedDiscord = false;
	private watchedEpisodeHash: { [s: string]: boolean } = {};
	private watchedEpisodeCount: number = 0;
	
	private cacheTime: number = 1800;
	
	public setWatchedEpisode(key: string): void {
		if (this.watchedEpisodeHash[key] == null) {
			this.watchedEpisodeCount++;
			this.watchedEpisodeHash[key] = true;
		}
		this.Save();
	}
	
	public episodeWatched(): number {
		return this.watchedEpisodeCount;
	}
	
	public setAsked() {
		this.askedDiscord = true;
		this.Save();
	}
	
	public async fetch(url: string): Promise<any> {
		let now: Date = new Date();
		if (this.fetchCache[url] != null) {
			if (now.getTime() - this.fetchCache[url].time < this.cacheTime * 1000) {
				return this.fetchCache[url].data;
			}
		}
		let data: Response = await fetch(url);
		let ret: any = await data.json();
		this.fetchCache[url] = {time: now.getTime(), data: ret};
		this.Save();
		return ret;
	}
	
	public setSubscribe(uuid: string, subscribed: boolean): void {
		this.episodeSubscribed[uuid] = subscribed;
		this.Save();
	}
	
	public asked(): boolean {
		return this.askedDiscord;
	}
	
	public getSubscribe(uuid: string): boolean {
		return this.episodeSubscribed[uuid] || false;
	}
	
	public setEpisodeVisited(uuid: string, episode: number): void {
		let a = this.episodeVisited[uuid];
		if (a == null) this.episodeVisited[uuid] = {};
		this.episodeVisited[uuid][episode] = true;
		this.Save();
	}
	
	public isEpisodeVisited(uuid: string, episode: number): boolean {
		let a = this.episodeVisited[uuid];
		if (a == null) this.episodeVisited[uuid] = {};
		let ep = this.episodeVisited[uuid][episode];
		if (ep == null) this.episodeVisited[uuid][episode] = false;
		return this.episodeVisited[uuid][episode];
	}
	
	Load(): void {
		let serialized: string | null = (localStorage || this.lmock).getItem("AnimeData");
		
		this.episodeVisited = {};
		this.episodeSubscribed = {};
		this.fetchCache = {};
		this.askedDiscord = false;
		this.watchedEpisodeHash = {};
		this.watchedEpisodeCount = 0;
		
		if (serialized != null) {
			let data: any = JSON.parse(serialized);
			if (data.episodeSubscribed != null) this.episodeSubscribed = data.subscribed;
			if (data.episodeVisited != null) this.episodeVisited = data.visited;
			if (data.fetchCache != null) this.fetchCache = data.fetch;
			if (data.askedDiscord != null) this.askedDiscord = data.askedDiscord;
			if (data.watchedEpisodeHash != null) this.watchedEpisodeHash = data.watchedEpisodeHash;
			if (data.watchedEpisodeCount != null) this.watchedEpisodeCount = data.watchedEpisodeCount;
		}
	}
	
	Save(): void {
		let data = {
			visited: this.episodeVisited,
			subscribed: this.episodeSubscribed,
			fetch: this.fetchCache,
			askedDiscord: this.askedDiscord,
			watchedEpisodeHash: this.watchedEpisodeHash,
			watchedEpisodeCount: this.watchedEpisodeCount
		};
		(localStorage || this.lmock).setItem("AnimeData", JSON.stringify(data))
	}
}

export {LocalData};
