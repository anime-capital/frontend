import {Core} from "@kirinnee/core";

class Episode {
	private readonly _updated: Date;
	private readonly _name: string;
	private readonly _number: number;
	private readonly _mpd: string;
	private readonly _mkv: string;
	private readonly _mp4: string;
	
	constructor(core: Core, host: string, updated: string, name: string, num: number, mpd: string, mkv: string, mp4: string) {
		core.AssertExtend();
		this._number = num;
		this._updated = new Date(updated);
		this._name = name;
		this._mpd = `${host}${mpd.ReplaceAll("%2F", "/")}`;
		this._mkv = `${host}${mkv.ReplaceAll("%2F", "/")}`;
		this._mp4 = `${host}${mp4.ReplaceAll("%2F", "/")}`;
	}
	
	get Number(): number {
		return this._number;
	}
	
	get Updated(): Date {
		return this._updated;
	}
	
	get Name(): string {
		return this._name;
	}
	
	get Mpd(): string {
		return this._mpd;
	}
	
	get Mkv(): string {
		return this._mkv;
	}
	
	get Mp4(): string {
		return this._mp4;
	}
	
	
}

export {Episode};