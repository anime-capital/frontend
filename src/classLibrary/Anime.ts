class Anime {
	private readonly _id: string;
	private readonly _name: string;
	private readonly _image: string;
	private readonly _genre: string[];
	private readonly _rating: number;
	private readonly _synopsis: string[];
	private readonly _date: Date;
	private readonly _episode: number;
	
	constructor(id: string, name: string, genre: string[], rating: number,
	            synopsis: string[], date: string, episode: number, image: string) {
		this._id = id;
		this._name = name;
		this._genre = genre;
		this._rating = rating;
		this._synopsis = synopsis;
		this._date = new Date(date);
		this._episode = episode;
		this._image = image;
	}
	
	get Id(): string {
		return this._id;
	}
	
	get Name(): string {
		return this._name;
	}
	
	get Genre(): string[] {
		return this._genre;
	}
	
	get Rating(): number {
		return this._rating;
	}
	
	get Synopsis(): string[] {
		return this._synopsis;
	}
	
	get Date(): Date {
		return this._date;
	}
	
	get Episode(): number {
		return this._episode;
	}
	
	get Image(): string {
		return this._image;
	}
	
}

export {Anime};