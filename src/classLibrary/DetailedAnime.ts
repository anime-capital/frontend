import {Episode} from "./Episode";

class DetailedAnime {
	
	private readonly _id: string;
	private readonly _name: string;
	private readonly _image: string;
	private readonly _genre: string[];
	private readonly _rating: number;
	private readonly _synopsis: string;
	private readonly _episodes: Episode[];
	
	constructor(id: string, name: string, image: string, genre: string[], rating: number, synopsis: string, episodes: Episode[]) {
		this._id = id;
		this._name = name;
		this._image = image;
		this._genre = genre;
		this._rating = rating;
		this._synopsis = synopsis;
		this._episodes = episodes;
	}
	
	get Id(): string {
		return this._id;
	}
	
	get Name(): string {
		return this._name;
	}
	
	get Image(): string {
		return this._image;
	}
	
	get Genre(): string[] {
		return this._genre;
	}
	
	get Rating(): number {
		return this._rating;
	}
	
	get Synopsis(): string {
		return this._synopsis;
	}
	
	get Episodes(): Episode[] {
		return this._episodes;
	}
}

export {DetailedAnime};