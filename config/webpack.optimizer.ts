import {Options} from "webpack";
import UglifyJsPlugin from "uglifyjs-webpack-plugin";

let opti: Options.Optimization = {
	minimizer: [
		new UglifyJsPlugin({
			uglifyOptions: {
				compress: {
					drop_console: true,
					unsafe: true
				},
				output: {comments: false},
				toplevel: true
			}
		}),
	]
};

opti.splitChunks = {
	maxSize: 240000,
	name: true,
	cacheGroups: {
		shaka: {
			test: /[\\/]node_modules[\\/]shaka-player[\\/]/,
			name: 'shaka',
			chunks: 'all',
			priority: -10
		},
		commons: {
			test: /[\\/]node_modules[\\/]/,
			name: 'vendors',
			chunks: "all",
			priority: -20,
			reuseExistingChunk: true
		}
	}
};

export {opti};
